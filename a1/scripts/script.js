console.log("Hello World");

let trainer = {
	            name : "Kimberly"
	          , age  : 25
	          , level : "Pro"
	          , pokemon : "Pikachu"
	          , action : function(){
	          	console.log("Training my " + this.pokemon);
	          }
}

console.log("Hi I'm " + trainer.name + " and I'm a " + trainer['level'] + " level trainer. I'm currently ")

console.log(trainer.action());


function pokemon(name, element){
	this.name = name
	this.element = element
	this.tackle = function(target){
		console.log(this.name +" said hi to " +target.name);
	}

}
let balbasur =  new pokemon("Balbasur", "Water");
let charizard = new pokemon("Charizard", "Fire");
console.log(balbasur);
console.log(charizard);

balbasur.tackle(charizard);
charizard.tackle(balbasur);
